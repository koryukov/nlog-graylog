﻿namespace GrayNLog.Targets
{
    /// <summary>
    /// Interface of a target sending data through a network.
    /// </summary>
    public interface INetworkTarget
    {
        /// <summary>
        /// Destination host.
        /// </summary>
        string Host { get; set; }

        /// <summary>
        /// Destination port.
        /// </summary>
        int Port { get; set; }
    }
}
