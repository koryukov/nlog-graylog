﻿using GrayNLog.Transport;
using NLog;
using NLog.Common;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace GrayNLog.Targets
{
    /// <summary>
    /// NLog target to pass logging events to the Graylog via UDP.
    /// </summary>
    /// <seealso href="https://docs.graylog.org/en/4.0/pages/gelf.html#gelf-via-udp">GELF via UDP</seealso>
    [Target("UdpGelf")]
    public class UdpTarget : TargetWithContext, INetworkTarget
    {
        private UdpClient _client;

        /// <summary>
        /// Initializes an instance of the target.
        /// </summary>
        public UdpTarget()
            : base()
        {
            IncludeEventProperties = true;
            Port = 12201;
            CompressionThreshold = 512;
        }

        /// <inheritdoc/>
        [RequiredParameter]
        public string Host { get; set; }

        /// <inheritdoc/>
        public int Port { get; set; }

        /// <summary>
        /// Message size, exceeding which the compression effect is engaged, bytes.
        /// </summary>
        /// <remarks>
        /// The default value is 512 bytes.
        /// Use negative values to disable compression.
        /// </remarks>
        public int CompressionThreshold { get; set; }

        /// <inheritdoc/>
        protected override void InitializeTarget()
        {
            base.InitializeTarget();
            _client = new UdpClient(Host, Port)
            {
                CompressionThreshold = this.CompressionThreshold
            };
            InternalLogger.Debug("{0}[{1}]. Initialized for {3}:{4}.", this.GetType(), Name, Host, Port);
        }

        /// <inheritdoc/>
        protected override void Dispose(bool disposing)
        {
            _client?.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// Writes logging event to the log target.
        /// </summary>
        /// <param name="logEvent">Logging event.</param>
        protected override void Write(LogEventInfo logEvent)
        {
            try
            {
                IDictionary<string, object> properties = this.GetGelfProperties(logEvent);
                byte[] message = JsonSerializer.SerializeToUtf8Bytes(properties);
                bool success = _client.SendAsync(message).Result;

                if (!success)
                    InternalLogger.Error("{0}[{1}]. Sending is not succeed. {2}.", GetType(), Name, logEvent);
            }
            catch (Exception error)
            {
                InternalLogger.Error(error, "{0}[{1}]. Failed to send {2}.", GetType(), Name, logEvent);
            }
        }
    }
}
