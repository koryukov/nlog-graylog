﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Threading.Tasks;
using Client = System.Net.Sockets.UdpClient;

namespace GrayNLog.Transport
{
    /// <summary>
    /// It implements 
    /// <see href="https://docs.graylog.org/en/4.0/pages/gelf.html#gelf-via-udp">GELF via UDP</see>
    /// sending.
    /// </summary>
    class UdpClient : IDisposable
    {
        private const int MaxChunks = 128;
        private const int MaxChunkSize = 8192;
        private const int MessageHeaderSize = 12;
        private const int MessageIdSize = 8;
        private const int MaxMessageBodySize = MaxChunkSize - MessageHeaderSize;

        private readonly Client _client;
        private readonly Random _random;

        /// <summary>
        /// Message size, exceeding which the compression effect is engaged, bytes.
        /// </summary>
        public int CompressionThreshold { get; set; } = -1;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="host">Target host.</param>
        /// <param name="port">Target port.</param>
        public UdpClient(string host, int port)
        {
            _client = new Client(host, port);
            _random = new Random();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            _client?.Dispose();
        }

        /// <summary>
        /// Sends data to the destination server.
        /// </summary>
        /// <param name="message">Message to be sent.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>
        /// Asynchronous operation, returning <see langword="true"/> when success,
        /// and <see langword="false"/> in otherwise.
        /// </returns>
        /// <remarks>
        /// The method uses compression and chunking if the <paramref name="message"/>
        /// is too big to be sent in one datagram.
        /// </remarks>
        /// <exception cref="OperationCanceledException">The operation has been canceled.</exception>
        public async Task<bool> SendAsync(byte[] message, CancellationToken cancellationToken = default)
        {
            if (CompressionThreshold >= 0 && message.Length > CompressionThreshold)
                message = await CompressAsync(message, cancellationToken);

            bool success = true;
            foreach (byte[] chunk in Chunk(message))
            {
                cancellationToken.ThrowIfCancellationRequested();
                int count = await _client.SendAsync(chunk, chunk.Length);
                success = success && count == chunk.Length;
            }

            return success;
        }

        private static async Task<byte[]> CompressAsync(byte[] message, CancellationToken cancellationToken = default)
        {
            using (var outputStream = new MemoryStream())
            {
                using (var gzipStream = new GZipStream(outputStream, CompressionMode.Compress))
                {
                    await gzipStream.WriteAsync(message, 0, message.Length, cancellationToken);
                    await gzipStream.FlushAsync(cancellationToken);
                }
                return outputStream.ToArray();
            }
        }

        private IEnumerable<byte[]> Chunk(byte[] message)
        {
            if (message.Length < MaxChunkSize)
            {
                yield return message;
                yield break;
            }

            byte sequenceCount = (byte)Math.Ceiling(message.Length / (double)MaxMessageBodySize);
            if (sequenceCount > MaxChunks)
                yield break;

            byte[] messageId = GenerateMessageId();
            for (byte sequenceNumber = 0; sequenceNumber < sequenceCount; sequenceNumber++)
            {
                byte[] messageHeader = GetMessageHeader(sequenceNumber, sequenceCount, messageId);
                int chunkStartIndex = sequenceNumber * MaxMessageBodySize;
                int messageBodySize = Math.Min(message.Length - chunkStartIndex, MaxMessageBodySize);
                byte[] chunk = new byte[messageBodySize + MessageHeaderSize];

                Array.Copy(messageHeader, chunk, MessageHeaderSize);
                Array.ConstrainedCopy(message, chunkStartIndex, chunk, MessageHeaderSize, messageBodySize);

                yield return chunk;
            }
        }

        private byte[] GenerateMessageId()
        {
            var messageId = new byte[8];
            _random.NextBytes(messageId);
            return messageId;
        }

        private static byte[] GetMessageHeader(int sequenceNumber, int sequenceCount, byte[] messageId)
        {
            var header = new byte[MessageHeaderSize];
            header[0] = 0x1e;
            header[1] = 0x0f;

            Array.ConstrainedCopy(messageId, 0, header, 2, MessageIdSize);

            header[10] = Convert.ToByte(sequenceNumber);
            header[11] = Convert.ToByte(sequenceCount);

            return header;
        }
    }
}
