﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;

namespace GrayNLog.Test
{
    [TestClass]
    public class Common
    {
        [AssemblyCleanup]
        public static void Clean()
        {
            LogManager.Shutdown();
        }
    }
}
