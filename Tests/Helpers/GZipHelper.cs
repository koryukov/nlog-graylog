﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace GrayNLog.Test.Helpers
{
    static class GZipHelper
    {
        private readonly static byte[] headerTemplate = { 0x1f, 0x8b };

        public static bool IsCompressed(this byte[] data)
        {
            IEnumerable<byte> header = data.Take(headerTemplate.Length);
            return headerTemplate.SequenceEqual(header);
        }

        public static byte[] Decompress(this byte[] compressed)
        {
            using (var inputStream = new MemoryStream(compressed))
            using (var gzipStream = new GZipStream(inputStream, CompressionMode.Decompress))
            using (var outputStream = new MemoryStream())
            {
                gzipStream.CopyTo(outputStream);
                return outputStream.ToArray();
            }
        }
    }
}
