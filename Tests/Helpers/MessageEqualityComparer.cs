﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace GrayNLog.Test.Helpers
{
    /// <summary>
    /// The class implements comparison of log messages.
    /// </summary>
    /// <seealso href="https://stackoverflow.com/a/60592310/7914637">Original StackOverflow post</seealso>
    class MessageEqualityComparer : IEqualityComparer<JsonElement>
    {
        private const int MAX_DEPTH = Int32.MaxValue;
        private const string TIMESTAMP_FIELD = "timestamp";

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageEqualityComparer"/> class.
        /// </summary>
        public MessageEqualityComparer()
        {

        }

        /// <summary>
        /// Determines whether two JSON strings are equal.
        /// </summary>
        /// <param name="x">The first string to compare.</param>
        /// <param name="y">The second string to compare.</param>
        /// <returns>
        /// The method returns <see langword="true"/> if the JSON representations are equal
        /// and <see langword="false"/> otherwise.
        /// </returns>
        /// <exception cref="JsonException">JSON is invalid.</exception>
        public static bool AreEqual(string x, string y)
        {
            var comparer = new MessageEqualityComparer();
            using (JsonDocument jx = JsonDocument.Parse(x))
            using (JsonDocument jy = JsonDocument.Parse(y))
            {
                return comparer.Equals(jx.RootElement, jy.RootElement);
            }
        }

        /// <summary>
        /// Get hash code for the specified JSON string.
        /// </summary>
        /// <param name="json">JSON string.</param>
        /// <returns>A hash code for the <paramref name="json"/> string.</returns>
        /// <exception cref="JsonException">JSON is invalid.</exception>
        public static int GetHashCode(string json)
        {
            var comparer = new MessageEqualityComparer();
            using (JsonDocument document = JsonDocument.Parse(json))
            {
                return comparer.GetHashCode(document.RootElement);
            }
        }

        /// <inheritdoc/>
        public bool Equals(JsonElement x, JsonElement y)
        {
            if (x.ValueKind != y.ValueKind)
                return false;
            
            switch (x.ValueKind)
            {
                case JsonValueKind.Null:
                case JsonValueKind.True:
                case JsonValueKind.False:
                case JsonValueKind.Undefined:
                    return true;

                case JsonValueKind.Number:
                    return x.GetRawText() == y.GetRawText(); // 0.0 != 0.00

                case JsonValueKind.String:
                    return x.GetString() == y.GetString();

                case JsonValueKind.Array:
                    return x.EnumerateArray().SequenceEqual(y.EnumerateArray(), this);

                case JsonValueKind.Object:
                    var xPropertiesUnsorted = x.EnumerateObject().ToList();
                    var yPropertiesUnsorted = y.EnumerateObject().ToList();
                    if (xPropertiesUnsorted.Count != yPropertiesUnsorted.Count)
                        return false;
                    var xProperties = xPropertiesUnsorted.OrderBy(p => p.Name, StringComparer.Ordinal);
                    var yProperties = yPropertiesUnsorted.OrderBy(p => p.Name, StringComparer.Ordinal);
                    foreach (var (px, py) in xProperties.Zip(yProperties))
                    {
                        if (px.Name != py.Name)
                            return false;
                        if (String.Equals(px.Name, TIMESTAMP_FIELD))
                        {
                            if (px.Value.ValueKind != JsonValueKind.Number || py.Value.ValueKind != JsonValueKind.Number)
                                return false;
                            continue;
                        }
                        if (!Equals(px.Value, py.Value))
                            return false;
                    }
                    return true;

                default:
                    throw new JsonException($"Unknown JsonValueKind {x.ValueKind}");
            }
        }

        /// <inheritdoc/>
        public int GetHashCode(JsonElement obj)
        {
            var hash = new HashCode();
            ComputeHashCode(obj, ref hash);
            return hash.ToHashCode();
        }

        /// <summary>
        /// Add values to hash code function.
        /// </summary>
        /// <param name="obj">Value to add.</param>
        /// <param name="hash"></param>
        /// <param name="depth">Current depth of recursion.</param>
        private void ComputeHashCode(JsonElement obj, ref HashCode hash, int depth = 0)
        {
            hash.Add(obj.ValueKind);

            switch (obj.ValueKind)
            {
                case JsonValueKind.Null:
                case JsonValueKind.True:
                case JsonValueKind.False:
                case JsonValueKind.Undefined:
                    break;

                case JsonValueKind.Number:
                    hash.Add(obj.GetRawText());
                    break;

                case JsonValueKind.String:
                    hash.Add(obj.GetString());
                    break;

                case JsonValueKind.Array:
                    if (depth != MAX_DEPTH)
                        foreach (var item in obj.EnumerateArray())
                            ComputeHashCode(item, ref hash, depth + 1);
                    else
                        hash.Add(obj.GetArrayLength());
                    break;

                case JsonValueKind.Object:
                    foreach (var property in obj.EnumerateObject().OrderBy(p => p.Name, StringComparer.Ordinal))
                    {
                        hash.Add(property.Name);
                        if (depth != MAX_DEPTH)
                        {
                            if (!String.Equals(TIMESTAMP_FIELD, property.Name))
                                ComputeHashCode(property.Value, ref hash, depth + 1);
                            else
                                hash.Add(property.Value.ValueKind);
                        }
                    }
                    break;

                default:
                    throw new JsonException($"Unknown JsonValueKind {obj.ValueKind}");
            }
        }
    }
}
