# Contribution Guide

Please take a moment to review this document in order to make the contribution
process easy and effective for everyone involved.

The key words *MUST*, *MUST NOT*, *REQUIRED*, *SHALL*, *SHALL NOT*, *SHOULD*,
*SHOULD NOT*, *RECOMMENDED*,  *MAY*, and *OPTIONAL* in this document are to be
interpreted as described in [RFC 2119][requirement-levels].

There are several ways to help develop the **GrayNLog** library:

* [add a bug report](#bug-report);
* [make a feature request](#feature-request);
* [write a code](#coding), including:
  - [fix a bug](#hotfix);
  - [implement a feature](#add-feature).

## Bug report

A bug is a problem that is caused by the code in the repository.

_Before publishing a bug report, check the same bug is not
[already reported][issue-tracker].
You could extend, clarify, detail or ask to reopen existing issue._

It's strongly recommended to [create a bug report][new-bug-report] using
*Bug report* issue template.
Use ~"bug" label for the issue and provide following information:

* library version which produces the bug;
* dependent project [TFM];
* [runtime identifier][RID];
* steps to reproduce the bug, including [minimal and reproducible][mcve] code
  example;
* expected behavior;
* actual behavior;
* error messages and stack trace (if it's thrown);
* [internal log][inlog] lines related to the problem (use `Debug` level).

Please, avoid pictures of errors, exceptions or code snippets, use
[blockquotes][blockquote] and [code blocks][code-block] instead.
Be responsive after publishing the report - reply to any clarification requests.

## Feature request

_Before publishing a feature request, take a moment to find out whether your
idea fits with the scope and aims of the project.
Afterwards check the same feature is not [already requested][issue-tracker].
You could extend, clarify, detail or ask to reopen existing issue._

It's strongly recommended to [create a feature request][new-feature-request]
using *Feature request* issue template. Use ~"feature" label for the issue and
provide following information to convince developers implement the feature:

* context in which the feature will be used;
* desired outcome;
* benefits of the changes;
* negative effects of the changes;
* proposed solution;
* argument validation requirements;
* requirements for handling of probably exceptions.

Please, avoid pictures of code snippets, use [code blocks][code-block] instead.
Be responsive after publishing the report - reply to any clarification requests.

## Coding

_Before write a code make sure your idea fits with the scope and aims of
the project. Examine [opened issues][opened-issues] to find actual problems
requiring solution or add own issue to estimate the idea value. You should also
understand there is a risk to spend a lot of time to do something that will
never merged, so ask before start any significant work._

Use [forking workflow][forking-workflow] to contribute as described below.

1. Fork the repository, then clone your fork, and configure upstream:

    ```bash
    git clone <forked-repository-url>
    cd nlog-graylog
    git remote add upstream https://gitlab.com/koryukov/nlog-graylog.git
    ```

1. Create a new branch in the forked repository:

    ```bash
    git checkout <source-branch-name>
    git pull upstream <source-branch-name>
    git checkout -b <working-branch-name>
    ```

   Replace `<source-branch-name>` with `main` for critical hotfixes
   or `development` otherwise.

1. Add unit tests these pass if the feature's specifications are met or the bug
   is fixed.
1. Arrive at the tests passing by implementing the feature or fixing the bug.
1. Update documentation if necessary.
1. Make sure added and modified files are `UTF-8`-encoded to avoid content
   rendering problems on the GitLab.
1. Commit your changes in logical chunks.
   Use `git rebase` feature to tidy up your commits before making them public. 
   Add informative messages in consider with [these recommendations][commit-messages].
1. Locally merge the upstream source branch into your current branch:

    ```bash
    git pull upstream <source-branch-name>
    ```

   Replace `<source-branch-name>` with `main` for critical hotfixes or
   `development` otherwise.

1. Push your topic branch up to your fork:

    ```bash
    git push origin <working-branch-name>
    ```

1. [Create a merge request][contribute-back] from your fork to contribute back
   to the upstream project. Hot fixes should target the `main` branch, others -
   the `development`.


[requirement-levels]: https://datatracker.ietf.org/doc/html/rfc2119
[TFM]: https://docs.microsoft.com/en-us/dotnet/standard/frameworks
[RID]: https://docs.microsoft.com/dotnet/core/rid-catalog
[mcve]: https://stackoverflow.com/help/minimal-reproducible-example
[inlog]: https://github.com/NLog/NLog/wiki/Internal-Logging
[blockquote]: https://docs.gitlab.com/ee/user/markdown.html#blockquotes
[code-block]: https://docs.gitlab.com/ee/user/markdown.html#code-spans-and-blocks
[commit-messages]: https://chris.beams.io/posts/git-commit/
[forking-workflow]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
[contribute-back]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-a-fork

[issue-tracker]: https://gitlab.com/koryukov/nlog-graylog/-/issues
[opened-issues]: https://gitlab.com/koryukov/nlog-graylog/-/issues?state=opened
[new-bug-report]: https://gitlab.com/koryukov/nlog-graylog/-/issues/new?issuable_template=Bug%20report
[new-feature-request]: https://gitlab.com/koryukov/nlog-graylog/-/issues/new?issuable_template=Feature%20request
