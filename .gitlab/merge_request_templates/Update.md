## Fixes

* ISSUE;
* ISSUE;
* ISSUE.

## Improvements

* ISSUE;
* ISSUE;
* ISSUE.

## Checklist

* [ ] the bugs and improvements are covered by tests;
* [ ] the bugs are fixed;
* [ ] all tests are succeeded;
* [ ] new version number of `GrayNLog.Test.dll` is incremented
      in consider with changes of the assembly API;
* [ ] `GrayNLog.Test.dll` version is stable and `VersionSuffix` is empty;
* [ ] documentation is updated and uses new version numbers;
* [ ] CI/CD pipelines are succeeded.
