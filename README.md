[![Semantic versioning][semver-badge]][semver]
[![NuGet][nuget-badge]][nuget-list]
[![License][license-badge]][license]
[![Target Framework][tfm-badge]][netstandard]
[![Coverage report][coverage-badge]][test-report]

# GrayNLog library

The **GrayNLog** is a library allows to write log messages to [Graylog] through
[NLog] platform.

### Quick start

1. Make sure your Graylog server is ready to [receive messages][graylog-settings]
   via UDP. 
1. Install the library using NuGet:

   ```powershell
   Install-Package GrayNLog -Version 2.0.0
   ```

1. Configure NLog to send [GELF] messages using a target provided by the library.
   Use one of the following examples to get started:

   * [`UdpGelf` target configuration][udp-config-example];
   * [`AsyncUdpGelf` target configuration][async-udp-config-example].
   
1. [Write][log-messages] log messages.

## Versioning

To maintain backward compatibility and make changes transparency, the library
is maintained under the [Semantic Versioning 2.0.0][semver].

Use [tags list][version-list] or [releases][release-list] to find a specific
version of the library.

## Contributing

Please take a moment to review [Contribution Guide][contribution-guide] in order
to make the contribution process easy and effective for everyone involved.


[NLog]: https://nlog-project.org/
[Graylog]: https://www.graylog.org/
[GELF]: https://docs.graylog.org/en/latest/pages/gelf.html
[semver]: https://semver.org/
[`TargetWithContext`]: https://nlog-project.org/documentation/v4.5.0/html/T_NLog_Targets_TargetWithContext.htm

[graylog-settings]: https://docs.graylog.org/en/latest/pages/getting_started/collect.html
[log-messages]: https://github.com/NLog/NLog/wiki/Tutorial#writing-log-messages

[contribution-guide]: CONTRIBUTING.md
[license]: LICENSE
[version-list]: https://gitlab.com/koryukov/nlog-graylog/-/tags
[release-list]: https://gitlab.com/koryukov/nlog-graylog/-/releases
[udp-config-example]: https://gitlab.com/koryukov/nlog-graylog/-/snippets/2134350
[async-udp-config-example]: https://gitlab.com/koryukov/nlog-graylog/-/snippets/2159470

[semver-badge]: https://img.shields.io/badge/semver-2.0.0-informational?logo=semver
[nuget-badge]: https://img.shields.io/nuget/v/graynlog?logo=nuget
[nuget-list]: https://www.nuget.org/packages/GrayNLog/latest
[license-badge]: https://img.shields.io/badge/license-Appache%202.0-informational?logo=opensourceinitiative&logoColor=fff
[coverage-badge]: https://gitlab.com/koryukov/nlog-graylog/badges/main/coverage.svg
[test-report]: https://gitlab.com/koryukov/nlog-graylog/-/jobs/artifacts/main/browse?job=test
[tfm-badge]: https://img.shields.io/badge/Framework-netstandard2.0-informational?logo=dotnet
[netstandard]: https://docs.microsoft.com/dotnet/standard/net-standard